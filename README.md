#Readme
This readme is for the project "overcrowding" by Guus Klinkenberg, which is based on [a project by Guus Klinkenberg and Laura Baakman](https://bitbucket.org/lauraBaakman/netcomputing2)

The project is meant as a project/Proof of Concept for the principle of detecting overcrowding (too many people in a area that is too small) using various webtechnologies, such as servlets and a RMI computing engine, and relies on current mobile technology (namely smartphones with a GPS sender).

To use the code, download/clone the repository and place the folders "src" and "web" in the project folder for a netbeans project which was created using the name "overcrowding" using the /overcrowding domain. This is important as a lot of files rely on this linkage.

##Run
	1. Start rabbitmq-server
	2. Start tomcat and load the server and servlet into it.
	3. Start the class Broker
	4. Start as many Clients as you wish

The heatmap should be displayed on your $serveraddress/netcomputing/heatmap.html

The client can be found at [bitbucket.com/GKlinkenberg/overcrowdingclient](https://bitbucket.org/GKlinkenberg/overcrowdingclient/overview)

The broker can be found at [bitbucket.com/GKlinkenberg/overcrowdingbroker](https://bitbucket.org/GKlinkenberg/overcrowdingbroker/overview)
package servlet;

import heatMap.HeatChart;

import datamodel.Locations;
import datamodel.Location;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import java.io.StringReader;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.awt.Color;

/**
 * Program that will get data from a server and generate a heatmap from it.
 *
 * REQUIRES THE FOLDER web/images TO BE PRESENT
 *
 * The program will get the data from the server using REST and will make sure
 * that the server deletes the data when this program has received it. This will
 * prevent clients, who are not actively sending their data, from showing in the
 * heatmap.
 *
 * @author Guus Klinkenberg & Laura Baakman
 */
public class HeatMapGenerator {

    /**
     * The amount of time the program will sleep between generating heatmaps.
     */
    private final int sleepPeriod = 1000;

    /**
     * The main function of the program.
     *
     * This function will first initialize all the objects needed for the
     * communication and then start getting the data, parsing those to the
     * proper format for the HeatChart library and will call the generation of
     * the heatmap and let it save to a file in the web/images folder.
     */
    public void run() {
        HeatChart map;
        String XML;
        StringReader reader;
        WebResource service;
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        service = client.resource(getBaseURI());

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Locations.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

            while (true) {
                XML = service.path("rest").path("locations").accept(MediaType.TEXT_XML).get(String.class);
                service.path("rest").path("locations").accept(MediaType.TEXT_XML).delete();
                reader = new StringReader(XML);
                Locations emps = (Locations) jaxbUnmarshaller.unmarshal(reader);

                double data[][] = new double[51][51];
                for (Location packet : emps.getLocations()) {
                    data[packet.getIntX()][packet.getIntY()]++;
                }

                map = new HeatChart(data);
                map.setLowValueColour(Color.green);
                map.setHighValueColour(Color.red);
                map.setShowXAxisValues(false);
                map.setShowYAxisValues(false);
                try {
                    map.saveToFile(new File("./web/images/java-heat-chart.png"));
                } catch (IOException ex) {
                    Logger.getLogger(HeatMapGenerator.class.getName()).log(Level.SEVERE, null, ex);
                }
                System.out.println("Generated a HeatMap with " + emps.getLocations().size() + " element(s). Going to sleep now.");
                try {
                    Thread.sleep(sleepPeriod);
                } catch (InterruptedException ex) {
                    Logger.getLogger(HeatMapGenerator.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (JAXBException ex) {
            Logger.getLogger(HeatMapGenerator.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    /**
     * Starts the HeatMapGenerator.
     *
     * @param args Isn't used.
     */
    public static void main(String[] args) {
        new HeatMapGenerator().run();
    }

    /**
     * The URL from which the REST service can be called.
     *
     * @return
     */
    private static URI getBaseURI() {
        return UriBuilder.fromUri("http://localhost:8080/netcomputing/").build();
    }
}

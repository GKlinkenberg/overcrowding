package client;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import datamodel.GPSPoint;
import datamodel.Location;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Program that simulates a device with GPS that sends its location.
 *
 * @author Guus Klinkenberg & Laura Baakman
 */
public class Client implements Runnable {

	final private String serverAddress = "localhost";
	final private int port = 7983;
	/**
	 * The value is about 5 meters in coordinate system.
	 */
	final private double maxOffsetCoords = 0.000045;
	final private int maxOffsetMeters = 3;
	/**
	 * Distance is in meters
	 */
	final private int rasterSize = 1;
	private ArrayList<UUID> ids;
	private GPSPoint sw;
	private GPSPoint ne;
	private GPSPoint clientLocation;
	private final static String QUEUE_NAME = "GPSLocation";
	private ConnectionFactory factory;
	private AMQP.BasicProperties.Builder builder;
	private static Long sleepPeriod = new Long(1 * 1000);

	/**
	 * Constructor that generates a random location on a 50 by 50 grid.
	 */
	public Client() {
		this(1);
	}
	
	public Client(int amountClients){
		this(GPSPoint.getRandomLocationInArea(new GPSPoint(0, 0), new GPSPoint(50, 50)), new GPSPoint(0, 0), new GPSPoint(50, 50), amountClients);
	}

	/**
	 * Constructor which gives the most control and invokes the initialization.
	 *
	 * @param clientLocation location of the client at the moment. May not be
	 * null
	 * @param southWest Southwest corner of the area the client needs to stay in
	 * to send data to the server.
	 * @param northEast Northeast corner of the area the client needs to stay in
	 * to send data to the server.
	 * @param amountClients The amount of clients to be simulated
	 */
	public Client(GPSPoint clientLocation, GPSPoint southWest, GPSPoint northEast, int amountClients) {
		initialize(clientLocation, southWest, northEast, amountClients);
		factory = new ConnectionFactory();
		factory.setHost(serverAddress);
		builder = new AMQP.BasicProperties().builder();
		builder.type("GPSLocation");
		builder.expiration(sleepPeriod.toString());
	}

	/**
	 * Sets the attributes of the class.
	 *
	 * @param clientLocation Location where the client is.
	 * @param southWest Southwest corner of the area the client needs to stay in
	 * to send data to the server.
	 * @param northEast Northeast corner of the area the client needs to stay in
	 * to send data to the server.
	 */
	private void initialize(GPSPoint clientLocation, GPSPoint southWest, GPSPoint NorthEast, int amountClients) {
		this.clientLocation = clientLocation;
		sw = southWest;
		ne = NorthEast;
		ids = new ArrayList<UUID>();
		for (int i = 0; i < amountClients; ++i) {
			ids.add(UUID.randomUUID());
		}
	}

	/**
	 * Main method for Client starts a client
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length > 1) {
			try {
				int firstArg = Integer.parseInt(args[0]);
				new Client(firstArg).run();
			} catch (NumberFormatException e) {
				System.err.println("Argument" + args[0] + " must be an integer.");
				System.exit(1);
			}
			
		} else {
			new Client().run();
		}
	}

	/**
	 * Generates a Location object and sends it to the predetermined queue.
	 */
	private void sendData() {
		for (UUID id : ids) {
			System.out.println(id);
			try {
				Connection connection = factory.newConnection();
				Channel channel = connection.createChannel();

				channel.queueDeclare(QUEUE_NAME, false, false, false, null);
				builder.timestamp(new Date());

				Location x = new Location(id, clientLocation.locationOnRaster(sw, rasterSize));

				channel.basicPublish("", QUEUE_NAME, builder.build(), x.getBytes());
				System.out.println(" [x] Sent '" + x + "'");

				channel.close();
				connection.close();

				System.out.println("Sent new location to queue");
			} catch (IOException ex) {
				ex.printStackTrace();
				Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	/**
	 * Infinite loop with : - Check within the defined area, if not, sleep for
	 * an iteration - Connect to the server - Convert it's coordinates to an
	 * index on the raster for the heatmap - Send these coordinates to the
	 * server.
	 */
	@Override
	public void run() {
		while (true) {
			System.out.println("In the while true");
			clientLocation = clientLocation.getRandomLocationNear(maxOffsetMeters);

			if (clientLocation.onLocation(sw, ne)) {
				sendData();
			} else {
				String msg = "Client was not in area between " + sw + " and " + ne + " with location " + clientLocation;
			}

			try {
				Thread.sleep(sleepPeriod);
			} catch (InterruptedException ex) {
				Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
				ex.printStackTrace();
			}
		}
	}
}

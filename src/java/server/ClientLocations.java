package server;

import java.util.HashMap;
import java.util.Map;
import datamodel.Location;

/**
 * Singleton design pattern The possible implementation of Java depends on the
 * version of Java you are using. As of Java 6 you can define singletons with a
 * single-element enum type. This way is currently the best way to implement a
 * singleton in Java 1.6 or later according to the book ""Effective Java from
 * Joshua Bloch.
 *
 * Class is blatantly copied and modified from the example given by Wico Mulder
 * from the Net-computing class at the University of Groningen.
 */
public enum ClientLocations {

    instance;
    private Map<String, Location> contentProvider = new HashMap<String, Location>();

    private ClientLocations() {
    }

    /**
     * @return Map with all the known locations of the clients using the ID's
     * from clients as key
     */
    public Map<String, Location> getModel() {
        return contentProvider;
    }

    /**
     * Removes all the locations from the Map.
     */
    public void empty() {
        this.contentProvider.clear();
    }
}

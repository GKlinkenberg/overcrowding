package server;

import datamodel.Locations;
import datamodel.Location;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.core.Response;

import javax.xml.bind.JAXBElement;

import java.util.ArrayList;
import java.util.List;

/**
 * REST interface for the ClientLocations.
 *
 * This class functions as a interface that everyone can approach using REST for
 * the ClientLocation object.
 *
 * @author Guus Klinkenberg & Laura Baakman
 */
@Path("/locations")
public class LocationsResource {

    @Context
    private UriInfo uriInfo;
    @Context
    private Request request;

    /**
     * TEXT_XML GET function.
     *
     * @return The stored locations in an ArrayList.
     */
    @GET
    @Produces(MediaType.TEXT_XML)
    public List<Location> getLocationsBrowser() {
        List<Location> locations = new ArrayList<Location>();
        locations.addAll(ClientLocations.instance.getModel().values());
        return locations;
    }
	
	@GET
    @Path("/json")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Location> getLocationsJSON() {
		List<Location> locations = new ArrayList<Location>();
        locations.addAll(ClientLocations.instance.getModel().values());
		return locations;
	}

    /**
     * APPLICATION_XML and APPLICATION_JSON GET function that's used by
     * application to get the locations from ClientLocations in a Locations
     * object.
     *
     * @return Stored locations in a Locations object.
     */
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Locations getLocations() {
        Locations locs = new Locations();
        locs.setLocations(new ArrayList<Location>(ClientLocations.instance.getModel().values()));
        return locs;
    }

    /**
     * Stores the location in the ClientLocations.
     *
     * @param newLoc Location that has to be stored
     * @return HTTP Response that will inform the client about how the put went.
     */
    @PUT
    @Consumes(MediaType.APPLICATION_XML)
    public Response putLocation(JAXBElement<Location> newLoc) {
        Location c = newLoc.getValue();
        return putAndGetResponse(c);
    }

    private Response putAndGetResponse(Location l) {
        Response res;
        if (ClientLocations.instance.getModel().containsKey(l.getId())) {
            res = Response.noContent().build();
        } else {
            res = Response.created(uriInfo.getAbsolutePath()).build();
        }
        ClientLocations.instance.getModel().put(l.getIdAsString(), l);
        return res;
    }

    /**
     * REST call that deletes the contents of the ClientLocations.
     */
    @DELETE
    public void deleteLocations() {
        ClientLocations.instance.empty();
    }

	@GET
    @Path("/delete")
	public void deleteLocationsBrowser() {
		ClientLocations.instance.empty();
	}
	
    /**
     * @return The amount of elements in the ClientsLocation.
     */
    @Produces(MediaType.TEXT_PLAIN)
    public String getCount() {
        int count = ClientLocations.instance.getModel().size();
        return String.valueOf(count);
    }
}

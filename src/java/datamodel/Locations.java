package datamodel;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Storage object for locations that is used to transfer them between the server
 * and the servlet using REST.
 *
 * @author Guus Klinkenberg & Laura Baakman
 */
@XmlRootElement(name = "locations")
@XmlAccessorType(XmlAccessType.FIELD)
public class Locations {

    @XmlElement(name = "location")
    private List<Location> locations = null;

    /**
     * @return List with all the locations stored in this object
     */
    public List<Location> getLocations() {
        return locations;
    }

    /**
     * Overwrites the existing locations with the given locations
     *
     * @param locations List with Locations of clients.
     */
    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

}

CREATE DATABASE netcomputing;

USE netcomputing;

CREATE TABLE geodata (
	time TIMESTAMP NOT NULL,
	id INT NOT NULL,
	longx DOUBLE NOT NULL,
	laty DOUBLE NOT NULL,
	PRIMARY KEY (time, id)
);

CREATE USER 'netcomputing'@'localhost' IDENTIFIED BY 'netcomputingpass';

GRANT INSERT, SELECT, UPDATE ON netcomputing.geodata TO 'netcomputing'@'localhost';
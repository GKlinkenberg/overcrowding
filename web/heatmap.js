var map, pointarray, heatmap;

var heatmapData = [
	new google.maps.LatLng(37.782551, -122.445368),
	new google.maps.LatLng(37.782745, -122.444586),
	new google.maps.LatLng(37.782842, -122.443688)
	];

function initialize() {

	var http = new XMLHttpRequest();
	http.open("GET", "http://localhost:8080/netcomputing/rest/locations/json", false);
	http.setRequestHeader('Accept', 'application/json');
	http.send();

	console.log(http.responseText);

	var jsonResponse = JSON.parse(http.responseText);
	console.log(jsonResponse.location[0].id);

	locs = jsonResponse.location;

	for(var i = 0; i < locs.length; ++i){
		console.log(locs[i]);
		console.log(locs[i].x);
		console.log(locs[i].y);
		heatmapData.push(new google.maps.LatLng(locs[i].x,locs[i].y))
	}

	// console.log(jsonResponse);

	var mapOptions = {
		zoom: 10,
		center: new google.maps.LatLng(53, 6),
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	map = new google.maps.Map(document.getElementById('map-canvas'),
			mapOptions);

	var pointArray = new google.maps.MVCArray(heatmapData);

	heatmap = new google.maps.visualization.HeatmapLayer({
		data: pointArray
	});

	heatmap.setMap(map);


}

function toggleHeatmap() {
	heatmap.setMap(heatmap.getMap() ? null : map);
}

function changeGradient() {
	var gradient = [
		'rgba(0, 255, 255, 0)',
		'rgba(0, 255, 255, 1)',
		'rgba(0, 191, 255, 1)',
		'rgba(0, 127, 255, 1)',
		'rgba(0, 63, 255, 1)',
		'rgba(0, 0, 255, 1)',
		'rgba(0, 0, 223, 1)',
		'rgba(0, 0, 191, 1)',
		'rgba(0, 0, 159, 1)',
		'rgba(0, 0, 127, 1)',
		'rgba(63, 0, 91, 1)',
		'rgba(127, 0, 63, 1)',
		'rgba(191, 0, 31, 1)',
		'rgba(255, 0, 0, 1)'
	]
	heatmap.set('gradient', heatmap.get('gradient') ? null : gradient);
}

function changeRadius() {
	heatmap.set('radius', heatmap.get('radius') ? null : 20);
}

function changeOpacity() {
	heatmap.set('opacity', heatmap.get('opacity') ? null : 0.2);
}

google.maps.event.addDomListener(window, 'load', initialize);

function reload(){
    location.reload(true);
}

// window.setTimeout(reload, 6000);